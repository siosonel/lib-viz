/*
Purpose: Simple template for viz controls
	- This pattern uses element divs coded in the html document, e.g., index.php
	- Clicks are monitored at the control panel level and most events are routed 
		to the main function, where parameter values are re-read. This approach
		is different from monitoring change events for each element. This requires 
		filtering the event in case the click or bubbled event is not important.
		
		
Author: Edgar Sioson
Data: 2013-05-21
*/

var e4 = (function () {
	var D = {}	
	var viz = {}
	
	var default_dimens = {}
	var default_exclude =[]
	var default_data = ""	
	var wrappers = {}
	
	var mgr = esd3()
		.pathgen(esd3pathgen)
		.zoomOk(['plotArea','area'])
		.minMaxGrp({
			"grp": {"chart": []}, 
			"active": ['chart'] //the filter strings to use when determining applicable min-max ranges
		})
	
	$(document).ready(function () {
		$('#updateBtn').click(main)
		$('#resetBtn').click(reset)
		$('#toggleBtn').click(prevView)
		reset()
	})
	
	function main() {
		prevView = $.extend({},D)
		D.data = d3.csv.parse($('#data').val()) //parseCSV),		
				
		D.dimens = JSON.parse($('#dimens').val())
		D.elems = JSON.parse($('#elems').val())
		D.exclude = $('#exclude').val().split(",");
		
		D.cols = $('#cols').val().split(',')
		D.layout = JSON.parse($('#layout').val())
		
		for(var type in viz) viz[type]()
	}
	
	function reset() {
		$('#dimens').val(JSON.stringify(default_dimens, null, "  "))
		$('#data').val(default_data)
		$('#elems').val(JSON.stringify(viz.chart.default_elems(), null, "  "))
		$('#exclude').val(default_exclude.join(","))
		$('#cols').val(viz.heat.cols().join(","))
		$('#layout').val(JSON.stringify(viz.heat.layout(), null, "  "))
		main();
	}
	
	function prevView() {		
		if (!('dimens' in prevView)) prevView = $.extend({},D)
		
		$('#dimens').val(JSON.stringify(prevView.dimens, null, "  "))
		$('#exclude').val(prevView.exclude.join(","))
		$('#cols').val(prevView.cols.join(","))
		$('#layout').val(JSON.stringify(prevView.layout, null, "  "))
		
		var csv = "year,v1,v2,v3,v4,v5,v6\n";
		prevView.data.map(function (d) {
			csv += d.year+','+d.v1+','+d.v2+','+d.v3+','+d.v4+','+d.v5+','+d.v6+"\n"
		})
		
		$('#data').val(csv)
				
		prevView.elems.map(function (d) { d.dset = "[csv data]" })		
		$('#elems').val(JSON.stringify(prevView.elems, null, "  "))
		
		main();
	}
	
	function parseCSV(d) {
		return {"year": +d[0], "v1": +d[1], "v2": +d[2], "v3": +d[3]}
	}
	
	main.D = function () {return D}
	main.mgr = mgr
	main.reset = reset
	
	main.default_data = function (csvStr) {
		if (!arguments.length) return default_data
		
		default_data = csvStr
		return main
	}
	
	main.dimens = main.viz = function (obj) {
		if (!arguments.length) return default_dimens
		for(var prop in obj) viz[prop] = obj[prop]
		return main;
	}
	
	main.viz = function (obj) {
		if (!arguments.length) return viz
		for(var prop in obj) viz[prop] = obj[prop]
		return main;
	}
	
	main.wrappers = function (obj) {
		if (!arguments.length) return wrappers
		for(var wrapperId in obj) default_dimens[wrapperId] = obj[wrapperId] 		
		return main
	}
	
	return main	
})()