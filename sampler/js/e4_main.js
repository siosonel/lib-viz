/*
Purpose: Configure controls and viz functions;
	- Also add tooltip, legends, other functions as needed
Author: Edgar Sioson
Data: 2013-05-21
*/

e4
.default_data(
	"year,v1,v2,v3,v4,v5,v6\n"
	+"1970,6,2,3,3,5,1\n"
	+"1980,7,1,4,8,6,1\n"
	+"1990,3,2,9,1,4,2\n"
	+"2000,5,4,6,2,7,3\n"
)
.wrappers({
	"divA":{width: 300, height: 250, margin: {top: 5, right: 20, bottom: 20, left: 50}},
	"divB":{width: 300, height: 250, margin: {top: 5, right: 20, bottom: 20, left: 50}}	
})
.viz({
	chart: 
		e4_chart(e4),
		
	heat: 
		e4_heat(e4)
		.layout({ //110__35__2__20__79__90__32
			offset: 10, width: 35, gap: 2, textX: 23,
			labelOffset: -5, labelWidth: 90, labelX: 0,
			closeBtnX: 5
		})
		.cols(["v1","v2","v3","v4","v5","v6"])
})



//example tooltip fxn usage



//example legendfxn



