/*
Purpose: Simple template for chart type viz
Author: Edgar Sioson
Data: 2013-05-21
*/

function e4_chart(controller) {
	var mgr = controller.mgr	
	var D = controller.D()
	
	//Note preppedData is an array of chart objects,
	//so multiple charts could be bound to one wrapper or container div
	var preppedData = [{ 
		chartId: "chart_0",				
		elems: [], //these are series objects
		bindkey: function (d) {return d.id}, //series are added/removed based on the bindkey
		title: {
			className: "chartTitle",
			text: function (d,i) {return 'chart_'+ i} 
		},
		plotArea: {
			className: 'plotArea'
		},
		svg: {plotclip: 1},
		mask: 1,
		duration: 1000
	}]
	
	//these will be bound to the chart as series elements
	var default_elems = [{
			type: "line", 
			dset: "[csv data]", 
			//bindkey: function (d) {return d.id},				
			name: 'myline', 
			id: "myline", 
			className: 'line',				
			x: 'year', y: 'v1'
		},{
			type: "area", 
			dset: "[csv data]", 
			//bindkey: function (d) {return d.id},				
			name: 'myarea', 
			id: "myarea", 
			className: 'area',				
			x: "year", y0: "v2", y1: "v3"
		},{
			type: "pt_circle", 
			dset: "[csv data]",
			
			//bindkey: function (d) {return d.id},				
			name: 'myCircle', 
			id: "myCircle", 
			className: 'circle',		
			cx: "year", cy: "v4", r: 5
			//fill: "red"
		},{
			type: "pt_pathD", 
			dset: "[csv data]",
			
			//bindkey: function (d) {return d.id},				
			name: 'myDiamond', 
			id: "myDiamond", 
			className: 'diamond',				
			pathD: "M-6,0L0,-6L6,0L0,6Z", 
			x: "year", y: "v5"
			//fill: "red"
		},{
			type: "pt_pathD", 
			dset: "[csv data]",
			
			//bindkey: function (d) {return d.id},				
			name: 'myX', 
			id: "myX", 
			className: 'smallX',				
			pathD: "M-6,-4L-4,-6L0,-2L4,-6L6,-4L2,0L6,4L4,6L0,2L-4,6L-6,4L-2,0Z", 
			x: "year", y: "v6"
			//fill: "red"
		}]		
	
	//optional initialization after document loads
	//$(document).ready()
	
	//convention to use function name = 'main' to trigger optional data request/prep/rendering
	function main() {		
		var ext = prepElemData()
	
		mgr.dimens(D.dimens)
			.exclude(D.exclude)
			.minMaxCalc( [+ext[0], +ext[1]+1], 'reset')
		
		//plotted series data
		var charts = d3.select('#divA')
			.selectAll('.divA_chart')
			.data(preppedData) //, D.bindkey)
			
		charts.enter().append('div')
			.attr('id', function (d) {return d.chartId+'_divA'})
			.attr('class', 'divA_chart')
			.call(mgr)
			
		charts.exit()
			.transition()
			.duration(1000)
			//.style('opacity',0.01)
			//.delay(2000)
			.remove()
		
		charts.call(
			mgr//.dimens(D.dimens)
			//.exclude(D.exclude)
			.minMaxCalc( [+ext[0], +ext[1]+1], 'reset')
			.update
		) //mgr.update('divA')	
	}
	
	function prepElemData() {		
		var yearArr = []
			
		if (!D.dimens || !D.data.length) alert('Invalid dimensions and/or data.')
					
		if (D.exclude.length == 1 && !D.exclude[0]) D.exclude = []
		
		D.data.map(function (d) {yearArr.push(d.year)})
		D.elems.map(function (d) { d.dset = D.data })
		
		preppedData[0].elems = D.elems		
		return d3.extent(yearArr); 
	}
	
	main.default_elems = function (obj) {
		if (!arguments.length) return default_elems
		default_elems = obj
		return main
	}
		
	return main
} 