/*
Purpose: Simple template for heat-map type viz
Author: Edgar Sioson
Data: 2013-05-21
*/

function e4_heat(controller) {
	var mgr = controller.mgr
	var D = controller.D()
	
	var cols = []
	var layout = {}
	
	//Note preppedData is an array of heatmap objects,
	//so multiple heatmap could be bound to one wrapper or container div
	var preppedData = [{
		chartId: 'heat',				
		elems: [],
		//bindkey: function (d) {return d.id}, //bindkey for element series
		x: 0, y: 0,
		title: {
			className: "chartTitle",
			text: '', //addTitle//"<span class='chartMainTitle'> ... Ranks</span><br />"
		},
		plotArea: {
			className: 'plotArea'
		},
		svg: {className: 'heat_svg'},
		mask: 'none'
	}]
	
	//optional initialization after document loads
	//$(document).ready()
	
	//convention to use function name = 'main' to trigger optional data request/prep/rendering
	function main() {		
		prepRanksData()
	
		var heat = d3.select('#divB')
			.selectAll('.divB_chart')
			.data(preppedData, function (d) {return d.chartId}) 
			
		heat.enter().append('div')
			.attr('id', function (d) {return d.chartId+'_divB'})
			.attr('class', 'divB_chart')
			.call(mgr)
			
		heat.exit()
			.transition()
			.duration(1000)
			//.style('opacity',0.01)
			//.delay(2000)
			.remove()
		
		heat.call(mgr.update) //mgr.update('divB')
	}
	
	//sort columns and rows
	function prepRanksData() {
		D.ranks = {}
		D.rows = []
		
		var tempObj = {}, labelColData = []
		
		D.data.map(function (d) {	
			for(var colName in d) {
				if (colName != 'year') {
					if (!(colName in tempObj)) tempObj[colName] = []
					tempObj[colName].push([d.year, +d[colName]])
				}
				else labelColData.push(d.year)
			}
		})
		
		for(var colName in tempObj) {
			D.ranks[colName] = tempObj[colName].sort(function(a,b) {return b[1] - a[1]}); 
		}
		
		D.ranks[ D.cols[0] ].map(function (d) {D.rows.push(d[0])}); 
		D.rows.reverse(); 
		
		preppedData[0].elems = []
		
		//add columns for ranked rows
		for(var colName in D.ranks) {
			preppedData[0].elems.push(prepRanksElem(colName))	
		}
		
		//add a label column
		preppedData[0].elems.push(prepLabelCol(labelColData))
	}

	function prepRanksElem(colName) {
		var colData = D.ranks[colName],
			yr = colData[0][0]
		
		return {
			type: 'stack_v',
			dset: D.ranks[colName],
			bindkey: function (d) {return d[0] }, //bindkey for observation/datapoint
			
			name: colName, //+"_col", //+ colData[0][0], 
			id: colName, //+"_col", //+colData[0][0],
			className: "col",
						
			width: D.layout.width, height: 16, gap: layout.gap,
			
			translate: function (d,i) {
				var x = D.cols.indexOf(colName);
				if (x == -1) return 'translate(3000,50)';
				else return 'translate('+ (x*(D.layout.width + D.layout.gap) + D.layout.offset) +',50)'
			},
			
			fill: function (obs,i) {
				return 'hsla('+ 30*i +',50%,50%,0.4)'
			},				
			
			cellRect: {
				x: 5, y: function (obs,i) {
					var y = D.rows.indexOf(obs[0]);
					if (y == -1) return 3000;
					return (D.rows.length - y )*18 + 18
				},
				yInit: 3000,
				id: function (d,i) {return colName+"_"+ d[0]+"_"+i+'_rect'},
				className: 'smallRect'
			},
			
			cellText: {
				x: D.layout.textX, 
				y: function (obs,i) {
					var y = D.rows.indexOf(obs[0]);
					if (y == -1) return 3000;  //if (!i) console.log(obs[0]+" "+ rows[obs[0]])
					return (D.rows.length - y)*18 + 30
				}, 
				yInit: 3000,
				id: function (d,i) {return colName+"_"+ d[0]+"_"+i+"_text"},
				className: "cellText", 
				val: function (d,i) {return i+1} 
			},	
			
			colLabel: { //fxn will be used to attach a label to each rendered column
				fxn: function (g,d) { 
					g.append('text')
						.attr('x',23)
						.attr('y',30)
						.text(d.id)
				} 
			},
			
			duration: 1000,
			transSelect: {col: 1, cell: 1, text: 1}
		}
	}

	function prepLabelCol(labelColData) {
		return {
			type: 'stack_v',
			dset: labelColData,
			bindkey: function (d) {return d}, //bindkey for observation/datapoint
			
			name: 'labelCol', //+"_col", //+ colData[0][0], 
			id: 'labelCol', //+"_col", //+colData[0][0],
			className: "col",
						
			width: D.layout.labelWidth, height: 16, 
			translate: 'translate('+ D.layout.labelOffset +',50)',		
			fill: 'none',
			cellText: {
				x: D.layout.labelX, 
				y: function (obs,i) {
					var y = D.rows.indexOf(obs);
					if (y == -1) return 3000;  //if (!i) console.log(obs[0]+" "+ rows[obs[0]])
					return (D.rows.length - y)*18 + 30
				}, 
				yInit: 3000,
				id: function (d,i) {return "label_"+ d+"_"+i+"_text"},
				className: "cellText", 
				val: function (d) {return d} 
			},				
			duration: 1000,
			transSelect: {col: 1, cell: 0, text: 1}
		}
	}
	
	main.layout = function (obj) {
		if (!arguments.length) return layout
		
		for(var prop in obj) layout[prop] = obj[prop]
		return main
	}
	
	main.cols = function (arr) {
		if (!arguments.length) return cols
		cols = arr
		return main
	}
	
	return main
}

