var default_dimens = {
	"divA":{width: 300, height: 250, margin: {top: 5, right: 20, bottom: 20, left: 50}},
	"divB":{width: 300, height: 250, margin: {top: 5, right: 20, bottom: 20, left: 50}}	
};
	
var	default_data = "year,v1,v2,v3,v4,v5,v6\n"
	+"1970,6,2,3,3,5,1\n"
	+"1980,7,1,4,8,6,1\n"
	+"1990,3,2,9,1,4,2\n"
	+"2000,5,4,6,2,7,3\n";

var default_elems = [{
			type: "line", 
			dset: "[csv data]", 
			//bindkey: function (d) {return d.id},				
			
			name: 'myline', 
			id: "myline", 
			className: 'line',				
			x: 'year', y: 'v1'
		},{
			type: "area", 
			dset: "[csv data]", 
			//bindkey: function (d) {return d.id},				
			
			name: 'myarea', 
			id: "myarea", 
			className: 'area',				
			x: "year", y0: "v2", y1: "v3"
		},{
			type: "pt_circle", 
			dset: "[csv data]",			
			//bindkey: function (d) {return d.id},				
			
			name: 'myCircle', 
			id: "myCircle", 
			className: 'circle',		
			cx: "year", cy: "v4", r: 5
			//fill: "red"
		},{
			type: "pt_pathD", 
			dset: "[csv data]",			
			//bindkey: function (d) {return d.id},				
			
			name: 'myDiamond', 
			id: "myDiamond", 
			className: 'diamond',				
			pathD: "M-6,0L0,-6L6,0L0,6Z", 
			x: "year", y: "v5"
			//fill: "red"
		},{
			type: "pt_pathD", 
			dset: "[csv data]",			
			//bindkey: function (d) {return d.id},				
			
			name: 'myX', 
			id: "myX", 
			className: 'smallX',				
			pathD: "M-6,-4L-4,-6L0,-2L4,-6L6,-4L2,0L6,4L4,6L0,2L-4,6L-6,4L-2,0Z", 
			x: "year", y: "v6"
			//fill: "red"
		}]		

var default_exclude = []

var default_cols = ["v1","v2","v3","v4","v5","v6"]
var default_layout = { //110__35__2__20__79__90__32
			offset: 10, width: 35, gap: 2, textX: 23,
			labelOffset: -5, labelWidth: 90, labelX: 0,
			closeBtnX: 5
		}

var D = {}

var preppedData = {
	'chart': [{ 
		chartId: "chart_0",				
		elems: [],
		bindkey: function (d) {return d.id},
		title: {
			className: "chartTitle",
			text: function (d,i) {return 'chart_'+ i} 
		},
		plotArea: {
			className: 'plotArea'
		},
		svg: {plotclip: 1},
		mask: 1,
		duration: 1000
	}],
	'heat': [{
		chartId: 'heat',				
		elems: [],
		//bindkey: function (d) {return d.id}, //bindkey for element series
		x: 0, y: 0,
		title: {
			className: "chartTitle",
			text: '', //addTitle//"<span class='chartMainTitle'> ... Ranks</span><br />"
		},
		plotArea: {
			className: 'plotArea'
		},
		svg: {className: 'heat_svg'},
		mask: 'none' /*,
		clipPath: {
			id: 'clip', shapes: [{
				tag: 'rect', x: 0, y: 0, width: 200, height: 200			
			}]
		}*/
	}]
}

var mgr = esd3()
	.pathgen(esd3pathgen)
	.zoomOk(['plotArea','area'])
	.minMaxGrp({
		"grp": {"chart": [], "heat": []}, 
		"active": ['chart'] //the filter strings to use when determining applicable min-max ranges
	})
	
$(document).ready(reset)

function reset() {
	$('#dimens').val(JSON.stringify(default_dimens, null, "  "))
	$('#data').val(default_data)
	$('#elems').val(JSON.stringify(default_elems, null, "  "))
	$('#exclude').val(default_exclude.join(","))
	$('#cols').val(default_cols.join(","))
	$('#layout').val(JSON.stringify(default_layout, null, "  "))
	main();
}

function main() {
	D.data = d3.csv.parse($('#data').val()) //parseCSV),
	drawChart()	
	drawHeat()
}

function drawChart() {
	D.dimens = JSON.parse($('#dimens').val())
	D.elems = JSON.parse($('#elems').val())
	D.exclude = []
	
	var yearArr = [],
		exclude = $('#exclude').val().split(",");
		
	if (!D.dimens || !D.data.length) alert('Invalid dimensions and/or data.')
				
	if (exclude.length == 1 && !exclude[0]) D.exclude = []
	else exclude.map(function (d) {D.exclude.push(d.trim())})
	
	D.data.map(function (d) {yearArr.push(d.year)})
	D.elems.map(function (d) { d.dset = D.data })
	
	preppedData.chart[0].elems = D.elems
	
	var ext = d3.extent(yearArr); 
	
	mgr.dimens(D.dimens)
		.exclude(D.exclude)
		.minMaxCalc( [+ext[0], +ext[1]+1], 'reset')
	
	//plotted series data
	var charts = d3.select('#divA')
		.selectAll('.divA_chart')
		.data(preppedData.chart) //, D.bindkey)
		
	charts.enter().append('div')
		.attr('id', function (d) {return d.chartId+'_divA'})
		.attr('class', 'divA_chart')
		.call(mgr)
		
	charts.exit()
		.transition()
		.duration(1000)
		//.style('opacity',0.01)
		//.delay(2000)
		.remove()
	
	mgr.update('divA')	
}

function parseCSV(d) {
	return {"year": +d[0], "v1": +d[1], "v2": +d[2], "v3": +d[3]}
}

function drawHeat() {
	prepRanksData()
	
	var heat = d3.select('#divB')
		.selectAll('.divB_chart')
		.data(preppedData.heat, function (d) {return d.chartId}) 
		
	heat.enter().append('div')
		.attr('id', function (d) {return d.chartId+'_divB'})
		.attr('class', 'divB_chart')
		.call(mgr)
		
	heat.exit()
		.transition()
		.duration(1000)
		//.style('opacity',0.01)
		//.delay(2000)
		.remove()
	
	mgr.update('divB')
}

function prepRanksData() {
	D.ranks = {}
	D.cols = $('#cols').val().split(',')
	D.rows = []
	D.layout = JSON.parse($('#layout').val())
	var tempObj = {}, cols = [], labelColData = []
	
	D.data.map(function (d) {	
		for(var colName in d) {
			if (colName != 'year') {
				if (!(colName in tempObj)) tempObj[colName] = []
				tempObj[colName].push([d.year, +d[colName]])
			}
			else labelColData.push(d.year)
		}
	})
	
	for(var colName in tempObj) {
		D.ranks[colName] = tempObj[colName].sort(function(a,b) {return b[1] - a[1]}); 
	}
	
	D.ranks[ D.cols[0] ].map(function (d) {D.rows.push(d[0])}); 
	D.rows.reverse(); 
	
	preppedData.heat[0].elems = []
	
	//add columns for ranked rows
	for(var colName in D.ranks) {
		preppedData.heat[0].elems.push(prepRanksElem(colName))	
	}
	
	//add a label column
	preppedData.heat[0].elems.push(prepLabelCol(labelColData))
}

function prepRanksElem(colName) {
	var colData = D.ranks[colName],
		yr = colData[0][0]
	
	return {
		type: 'stack_v',
		dset: D.ranks[colName],
		bindkey: function (d) {return d[0] }, //bindkey for observation/datapoint
		
		name: colName, //+"_col", //+ colData[0][0], 
		id: colName, //+"_col", //+colData[0][0],
		className: "col",
					
		width: D.layout.width, height: 16, gap: layout.gap,
		
		colLabel: {
			fxn: function (g,d) {
				g.append('text')
					.attr('x',23)
					.attr('y',30)
					.text(d.id)
			} 
		},
		
		translate: function (d,i) {
			var x = D.cols.indexOf(colName);
			if (x == -1) return 'translate(3000,50)';
			else return 'translate('+ (x*(D.layout.width + D.layout.gap) + D.layout.offset) +',50)'
		},
		
		fill: function (obs,i) {
			return 'hsla('+ 30*i +',50%,50%,0.4)'
		},				
		//colLabel: {x: 0, y: 10, fxn: controller.labelHeatCol, id: cr+"_"+ row.iso3 +'_title', transform: 'rotate(320 -10,-5)'}, 
		cellRect: {
			x: 5, y: function (obs,i) {
				var y = D.rows.indexOf(obs[0]);
				if (y == -1) return 3000;
				return (D.rows.length - y )*18 + 18
			},
			yInit: 3000,
			id: function (d,i) {return colName+"_"+ d[0]+"_"+i+'_rect'},
			className: 'smallRect'
		},
		cellText: {
			x: D.layout.textX, 
			y: function (obs,i) {
				var y = D.rows.indexOf(obs[0]);
				if (y == -1) return 3000;  //if (!i) console.log(obs[0]+" "+ rows[obs[0]])
				return (D.rows.length - y)*18 + 30
			}, 
			yInit: 3000,
			id: function (d,i) {return colName+"_"+ d[0]+"_"+i+"_text"},
			className: "cellText", 
			val: function (d,i) {return i+1} 
		},				
		duration: 1000,
		transSelect: {col: 1, cell: 1, text: 1}
	}
}

function prepLabelCol(labelColData) {
	return {
		type: 'stack_v',
		dset: labelColData,
		bindkey: function (d) {return d}, //bindkey for observation/datapoint
		
		name: 'labelCol', //+"_col", //+ colData[0][0], 
		id: 'labelCol', //+"_col", //+colData[0][0],
		className: "col",
					
		width: D.layout.labelWidth, height: 16, 
		translate: 'translate('+ D.layout.labelOffset +',50)',		
		fill: 'none',
		cellText: {
			x: D.layout.labelX, 
			y: function (obs,i) {
				var y = D.rows.indexOf(obs);
				if (y == -1) return 3000;  //if (!i) console.log(obs[0]+" "+ rows[obs[0]])
				return (D.rows.length - y)*18 + 30
			}, 
			yInit: 3000,
			id: function (d,i) {return "label_"+ d+"_"+i+"_text"},
			className: "cellText", 
			val: function (d) {return d} 
		},				
		duration: 1000,
		transSelect: {col: 1, cell: 0, text: 1}
	}
}


