/*
Purpose: This is the core engine that renders charts for selected points within 
a dataset. 

Description: 
- The code and conventions were adopted from http://bost.ocks.org/mike/chart/.
- e4() binds nested data objects onto DOM elements
	- Chart level transitions, such as multiple charts entering or exiting a container div, 
		are not handled here, but instead handled at the viz application level
	- series enter, exits, updates are handled by corresponding esd3pathgen functions
		as called in the e4.update() function below
	- datapoint enter, exits, updates are handled by functions called within 
		the corresponding pathgen function
- the minMax() and zoom functions have been separated to clarify code organization 

Author: Edgar Sioson
Date: 2013-05-24
*/
var esd3 = e4; //map previous name

if (!('disjoin' in $)) $.disjoin = function(a, b) {
  return $.grep(a, function($e) { return $.inArray($e, b) == -1; });
}


/****************************************************
	Core function that binds nested data objects, plus 
	calls other functions to draw, update, or remove 
	chart, series, and datapoint element components
******************************************************/

function e4() { 
  //these variables are not visible outside of the core function (through closure),
	//unless exposed by an externally accessible function 
	var Styles = {"_": { //default chart dimensions
				width: 350, height: 300, margin: {top: 5, right: 20, bottom: 50, left: 50}
			}},      			
				
			legend = {include: [], fxn: function (g,legendArr) {}},  
			toolTip = {elem: [], fxn: function () {}},
			
			//axesTip = 0.05, //top-off axes length by this length ratio to 
			selected = {}, //array of elements belonging to the same wrapper div
			minMax = e4_minMax(selected),
			//zoom variable is declared before returning chart, since it's initialization requires additional functions to be attached to esd3
      pathgen = {}; //will be filled in externally through chart.pathgen() 
	
	//the main function that renders the chart
  function chart(selection) { 
		if (!selection.length) return; 
		
		var wrapperId = selection[0].parentNode.id,
			s = wrapperId in Styles ? Styles[wrapperId] : Styles._ ,
			height = s.height, width = s.width, margin = s.margin,
			effwidth = width - margin.left - margin.right,
			effheight = height - margin.top - margin.bottom;
		
		selected[ wrapperId ] = selection;
		minMax.calc(); 
	
		selection.each( function(data) {			
		//d3.select('#'+wrapperId).selectAll('.'+wrapperId+'_chart').each( function (data) {
			var chartId = data.chartId +"_"+ wrapperId,
				legendArr = [], yDomain = minMax.get(chartId, wrapperId); 
			
			//add chart title
			if (data.title && typeof data.title == "object") { 
				d3.select(this)
					.append("div")
						.attr("class", data.title.className)
						.html(data.title.text)
			}
			
			//create scales
			var xScale = d3.scale.linear().domain(minMax.x()).range([0, effwidth]), 							
			yScale = d3.scale.linear().domain( yDomain ).range([effheight, 0]);
			
			//create svg container
			var svg = d3.select(this)
				.append("svg")
					.datum(data)
					.attr('id', ('svg' in data) ? data.svg.id : null)
					.attr('class', ('svg' in data) ? data.svg.className : null)
					.attr("width", width)
					.attr("height", height)
					.on("mousedown.esd3", zoom)
			
			//add clipPath
			if (data.svg.plotclip) { 
				var clip = svg.append('defs').append('clipPath').attr('id', chartId+'_plotClip_'+wrapperId)
							
				clip.append('rect')
					//.attr("class", data.plotArea.className)
					.attr('x', data.x)
					.attr('y', data.y)
					.attr("width", effwidth)
					.attr("height", effheight)
					//.attr("transform", "translate(" + margin.left + "," + margin.top + ")")
			} 
			
			//wrapper g for translating whole plot area easily
			var g = svg.append("g") 
				.attr("class", "chart_svg")
				.attr("transform", "translate(" + margin.left + "," + margin.top + ")");			
			
			//add rectangle, a.k.a. stylable background, to capture clicks within plot area
			g.append("rect")
				.attr('id',chartId+'_plotArea')
				.attr("class", data.plotArea.className)
				.attr('x', data.x)
				.attr('y', data.y)
				.attr("width", effwidth)
				.attr("height", effheight)
			
			//add data series
			var dataSeries = g.append("g").attr("class","dataSeries") 
				.attr('clip-path', data.svg.plotclip ? 'url(#'+chartId+'_plotClip_'+ wrapperId +')' : '')
				.selectAll("g") 
				.data(data.elems, data.bindkey)
				.enter()
				.append("g")
					.attr("id", function (d) {return d.id +"_"+wrapperId })
					.each( function (d) { 			
						var g = d3.select(this)
						
						//Note that different wrapperIds will share the same scale functions
						//for the same data series; has to recalc domain & range on update
						if (!("xScale" in d)) d.xScale = xScale;	
						else d.xScale.domain(minMax.y()).range([0, effwidth])
						
						if (!("yScale" in d)) d.yScale = yScale;
						else d.yScale.domain( yDomain ).range([effheight, 0])
						
						if (minMax.excluded(d.id)) g.style("display", "none");																
						pathgen[d.type].draw(g, d, wrapperId)
						d.fxn = pathgen[d.type];

						if ("dset" in d && typeof d.className == 'string' && d.className.search('pt_hidden') == -1) legendArr.push(d)
					})		
						
			//mask data plotted outside the main chart area			
			if ('mask' in data && data.mask != "none") {
				g.append("rect")
				.attr("class", "rightMask")
				.attr("x", width - margin.left - margin.right)
				.attr("y", -10)
				.attr("width", margin.right)
				.attr("height", height)
						
				g.append("rect")
				.attr("class", "bottomMask")
				.attr("y", height - margin.top - margin.bottom)
				.attr("width", width - margin.left)
				.attr("height", margin.bottom)	
			
				g.append("rect")
				.attr("class", "leftMask")
				.attr("x", -1*margin.left)
				.attr("y", -10)
				.attr("width", margin.left)
				.attr("height", height)
			}
			
			//create axes
			this.xAxis = d3.svg.axis()
				.scale(xScale)
				.orient("bottom")
				.tickSize(6, 0)
				.ticks(5)
				.tickFormat(d3.format("d")),
			
			this.yAxis = d3.svg.axis()
				.scale(yScale)
				.orient("left")
				.tickSize(6, 0)
				.ticks(5)
				.tickFormat(d3.format("g")),
			
			g.append("g")
				.attr("class", "xAxis")
				.attr("transform", "translate(-0.5," + (effheight - 0.5) + ")")
				.call(this.xAxis);
				
			g.append("g")
				.attr("class", "yAxis")
				.attr("transform", "translate(-0.5,0)")
				.call(this.yAxis);	
			
			//create legend, would not do anything if there is no 
			//selection in legendArr or if legend.fxn is just function () {}
			legend.fxn(g, legendArr)
    });
  }
	
	//update the rendering of charts within a given wrapperId
	function update(arg1, pts, idStr) {		
		if (typeof arg1 == 'string') {
			var wrapperId = arg1,
				selection = d3.select('#'+wrapperId).selectAll('.'+wrapperId+'_chart')
		}
		else if (Array.isArray(arg1)) {
			var selection = arg1,
				wrapperId = selection[0].parentNode.id;
		}
		
		
		if (arguments.length == 1 || pts === 0) var pts = {}
	
		var s = wrapperId in Styles ? Styles[wrapperId] : Styles._ ,
		margin = s.margin, 
		effheight = s.height - margin.top - margin.bottom, 
		effwidth = s.width -  margin.left - margin.right; 
				
		//selected[wrapperId].each( function (data) { 
		selection.each( function (data) {
			//idStr is an additional filter for the ids of charts within a wrapper
			if (idStr && data.chartId.search(idStr) == -1) return; //when zooming in or out of a single chart
			
			var chartDiv = d3.select(this),
				svg = chartDiv.select('svg'),
				millisec = 'duration' in data ? data.duration : 1000,
				chartId = data.chartId +"_"+ wrapperId, //d3.select(this).datum(data).select('svg')
				legendArr = [];
		
			svg.transition().duration(millisec)
				.attr('height', s.height)
				.attr('width', s.width)
			
			var clip = svg.select('#'+chartId+'_plotClip_'+wrapperId).select('rect').transition().duration(millisec)							
			clip.attr('x', data.x)
			clip.attr('y', data.y)
			clip.attr("width", effwidth)
			clip.attr("height", effheight)
			
			svg.select('.chart_svg').select('#'+chartId+'_plotArea')
				.attr('x', data.x)
				.attr('y', data.y)
				.attr("width", effwidth)
				.attr("height", effheight)
			
			var rightMask = svg.selectAll(".rightMask").transition().duration(millisec)
				rightMask.attr("x", s.width - margin.left - margin.right)
				rightMask.attr("y", -10)
				rightMask.attr("width", margin.right)
				rightMask.attr("height", s.height)
						
			var bottomMask = svg.selectAll(".bottomMask").transition().duration(millisec)
				bottomMask.attr("y", s.height - margin.top - margin.bottom)
				bottomMask.attr("width", s.width - margin.left)
				bottomMask.attr("height", margin.bottom)	
			
			var leftMask = svg.selectAll(".leftMask").transition().duration(millisec)
				leftMask.attr("x", -1*margin.left)
				leftMask.attr("y", -10)
				leftMask.attr("width", margin.left)
				leftMask.attr("height", s.height)
			
			if (data.chartId in minMax.y().zoomed) domains = minMax.y().zoomed[data.chartId]
			else domains = {x: minMax.x(), y: minMax.get(chartId, wrapperId)} //default unzoomed			

			//Update the x-scale
			var xScale = d3.scale.linear()
				.domain( domains.x )
        .range([0, effwidth]);

			if ("x" in pts) {  
				domains.x = [ xScale.invert(pts.x[0] - margin.left) , xScale.invert(pts.x[1] - margin.left) ]; 
				xScale.domain( domains.x ) 
			}		
					
			if ('xAxis' in this) {
				var x = chartDiv.selectAll('.xAxis')
					.transition().duration(millisec)
				
				x.call( this.xAxis.scale(xScale) );
				x.attr("transform", "translate(-0.5," + (effheight - 0.5) + ")")
			}
					
      // Update the y-scale.
      var yScale = d3.scale.linear()
				.domain( domains.y ) 
        .range([effheight, 0]); 
		
			if ("y" in pts) { 
				domains.y = [ yScale.invert(pts.y[1] - margin.top), yScale.invert(pts.y[0]) ]
				yScale.domain( domains.y )
			}

			if ("x" in pts || "y" in pts) minMax.y().zoomed[data.chartId] = domains
			if (data.chartId in minMax.y().zoomed) { //add zoom reset button
				d3.select('#'+chartId)
					.append('div')
					.attr('class', 'zoomResetDiv')
					.selectAll('button')
					.data(['reset']) //['-','reset','+'])
					.enter()
					.append("button")
						.attr('class', "zoomResetBtn")
						.text( function (d) { return d; })
						.on('click.esd3', zoom.reset)
			}
			
			if ('yAxis' in this) {
				var y = chartDiv.selectAll('.yAxis')
					.transition().duration(millisec)
				
				y.call(this.yAxis.scale(yScale));
				y.attr( "transform", "translate(-0.5,0)" )
			}
			
			//bind new element data to dataseries 'g'
			var dataSeries = svg
				.select('.dataSeries').selectAll('g')
				.data(data.elems, data.bindkey)
			
			//add new alements and pass to pathgen function
			dataSeries.enter().append("g")
				.attr("id", function (d) {return d.id +"_"+wrapperId })
				.each(function (d) {
					var g = d3.select(this)
					
					//Note that different wrapperIds will share the same scale functions
					//for the same data series; has to recalc domain & range on update
					if (!("xScale" in d)) d.xScale = xScale;	
					else d.xScale.domain(minMax.x()).range([0, effwidth])
					
					if (!("yScale" in d)) d.yScale = yScale;
					else d.yScale.domain( domains.y ).range([effheight, 0])
					
					if (minMax.excluded(d.id)) g.style("display", "none");																
					pathgen[d.type].draw(g, d, wrapperId, 'update')
					d.fxn = pathgen[d.type];
					
					if ("dset" in d && typeof d.className == 'string' && d.className.search('pt_hidden') == -1) legendArr.push(d)
					//if ("dset" in d && d.className.search('pt_hidden') == -1) legendArr.push(d)
			})
					
			dataSeries.each( function (dObj, i) { 
				dObj.xScale = xScale;				
				dObj.yScale = yScale;
				
				var g = d3.select(document.getElementById(dObj.id+'_'+wrapperId))
									
				if (minMax.excluded(dObj.id)) g.style("display","none")
				else {			
					if (!('fxn' in dObj)) dObj.fxn = pathgen[dObj.type]; 
					
					g.style("display","block")				
					pathgen[dObj.type].update(g,dObj)					
				}
				
				if ("dset" in dObj && typeof dObj.className == 'string' && dObj.className.search('pt_hidden') == -1) legendArr.push(dObj)
			})	
			
			//pass removed element to pathgen function for removal or otherwise 
			dataSeries.exit().each(function (d) {
				pathgen[d.type].elemExit(d3.select(this), d, wrapperId) 
				legendArr = $.disjoin(legendArr, [d]); 
			})
		
			legend.fxn(svg, legendArr)
		})
		
		return chart;
	}
	
	chart.minMaxCalc = function (xRange, yRange) {
		minMax.calc(xRange,yRange);
		return chart;
	}
	
	chart.minMaxGrp = function (obj) { 
		if (!arguments.length) return minMax.get();
		minMax.grp(obj)		
		return chart;
	}
	
	//make update function available externally
	chart.update = update
	
	//get or set the closured Styles object
	chart.styles = function(wrapperId, obj) {
    if (!arguments.length) return Styles;
    if (!arguments.length == 1) return Styles[wrapperId];
		
		if (!Styles.hasOwnProperty(wrapperId)) Styles[wrapperId] = {}		
		for(var prop in obj) (Styles[wrapperId])[prop] = obj[prop];
		
		//fill in any missing style property for wrapperId
		for(var prop in Styles._) {
			if (!Styles[wrapperId].hasOwnProperty(prop)) (Styles[wrapperId])[prop] = Styles._[prop];
		}
		
		return chart;
  };
	
	//get or set wrapper dimensions
	chart.dimens = function (obj) {
		if (!arguments.length) return Styles;
		for (var prop in obj) Styles[prop] = obj[prop]
		return chart;
	}
	
	//get or set the closured exclude array
	chart.exclude = function(str, not) {
		if (!arguments.length) return minMax.exclude();
		minMax.exclude(str,not)		
		return chart;
	};
	
	chart.excluded = minMax.excluded; //function to find out if an lement is excluded from rendering
	
  chart.x = function(_) {
    if (!arguments.length) return xValue;
    xValue = _;
    return chart;
  };

  chart.y = function(_) {
    if (!arguments.length) return yValue;
    yValue = _;
    return chart;
  };
	
	//get or set the closured axesTip variable
	chart.axesTip = function(_) {
    if (!arguments.length) return axesTip;
    axesTip = _;
    return chart;
  };	
	
	//get or set the toolTip 
	chart.toolTip = function (arg) {
		if (!arguments.length) return toolTip.fxn;
		
		if ("elem" in arg && "fxn" in arg) {
			toolTip = arg; //set what fxn to use			
			chart.toolTipSet( arg.fxn ) //activate listening on mouseover
		}	
		
		return chart;
	}
	
	//activate tooltip fxn on mouseover
	chart.toolTipSet = function (fxn) { 
		toolTip.elem.map( function (d) {
			d3.select(d).on("mouseover", fxn)
		})
	}
	
	//get or set the closured legend object, fxn
	chart.legend = function (_) {
		if (!arguments.length) return legend;
    for(var prop in _) {
			legend[prop] = _[prop];
		}
		
		return chart;
	}
	
	chart.selected = function () {
		return selected;
	}
	
	chart.preppedFxns = function () {
		return preppedFxns;
	}
	
	chart.zoomOk = function (d) {
		if (!arguments.length) return zoom.ok();
		zoom.ok(d);	
		return chart;
	}
	
	chart.pathgen = function (_) {
		if (!arguments.length) return pathgen;
		
		for(var prop in _) pathgen[prop] = _[prop];		
		return chart;
	}
	
	var zoom = e4_zoom(minMax, chart)
	
  return chart;
}


/********************************************************** 
Function for calculating minimum, maximum for chart groups
***********************************************************/

function e4_minMax(selectionObj) {
	var selected = selectionObj, //reference to {wrapper-id: array of chart objects, ...} object when drawn by e4
		xMinMax = [0,1], //default x-range to be used by all the charts on view		
		yMinMax = { //these values are used to support dynamic rescaling based on desired user view
			"user": [0,1], "self":{}, "active": [], "grp": {},
			"checked": {}, "zoomed": {}, "override": {min: 0, max: 0, val: [0,0]}
		},
		
		exclude = [], //chart or series plot ids that have any of the excluded string will be hidden and not updated
		axesTip = 0.05;	
	
	function main() {}
	
	
	main.x = function () {return xMinMax}
	main.y = function () {return yMinMax}	
	
	main.exclude = function (str, not) {
		if (!arguments.length) return exclude
		
		if (Array.isArray(str)) exclude = str;
		else if (!not && exclude.indexOf(str) == -1) exclude.push(str)
		else if (not && exclude.indexOf(str) != -1) exclude.splice(  exclude.indexOf(str), 1)

		return main;
	}
	
	
	//determine if the elem with the given id should be excluded from
	//min-max calculation and not displayed
	main.excluded = function (id) { 
		for(var i=0; i<exclude.length; i++) { 
			if (id.search(exclude[i]) != -1) return 1;
		}
	}
	
	//this relies on plot functions to calculate the min-max of 
	//datapoints that it plots
	main.calc = function (xRange, yRange) {
		if (xRange && xRange.length == 2) xMinMax = xRange
		
		if (yRange && yRange != 'reset' && yRange.length == 2) yMinMax.user = yRange;
		else { 	
			for(var grpName in yMinMax.grp) yMinMax.grp[grpName] = []
			
			var checked = [] , i=0, 
				xRange = xMinMax.join(""), //will be used to track previous calculated values for the same range, to avoid recalculation
				filtersUsed = exclude.join(" "), //filtersUsed keeps track of filters applied when min-max was last calculated for chart
				reused = '';
			
			//data series should be the same for the same chart regadless of wrapper used
			for(var wrapperId in selected) filtersUsed = filtersUsed.replace(wrapperId,"")
		
			if (!yMinMax.checked.hasOwnProperty(xRange)) {
				delete yMinMax.checked; //do not want cached min-max value to baloon
				yMinMax.checked = {}
				yMinMax.checked[ xRange ] = {}
			}
			
			var yMC = yMinMax.checked[ xRange ] 

			for(var wrapperId in selected) {
				if (main.excluded(wrapperId)) {continue;} //skip this iteration
				
				//selected[wrapperId].each( function (data) { 
				d3.select('#'+wrapperId).selectAll('.'+wrapperId+'_chart').each( function (data) { 
					if (checked.indexOf(data.chartId) == -1) checked.push(data.chartId); 
					else return; //console.log(data.chartId)
							
					var chartId = data.chartId +"_"+ wrapperId, //yArr = [],
						yArr = [] //, yMax = -1, tempMinMax = []
				
					if (yRange != 'reset' && data.chartId in yMinMax.self && yMinMax.self[data.chartId][1]
						&& data.chartId in yMC && yMC[data.chartId] == filtersUsed) {
						yArr = yMinMax.self[ data.chartId ]; i++; reused = reused + "<br />\n"+ data.chartId;
					}
					else { 		
						yMC[data.chartId] = filtersUsed;
						
						data.elems.map( function (dObj) { //dObj = series data, one of possibly many in a chart
							if (main.excluded(dObj.id)) return; //if (!('dset' in dObj) || !dObj.dset.length) console.log("No dset: "+dObj.id)
						
							yArr = yArr.concat( esd3pathgen[dObj.type].minMax(xMinMax, 0, dObj) )							
						})
						
						yArr = d3.extent(yArr);   
						yMinMax.self[ data.chartId ] = yArr; 
					} 
					
					for(var grpName in yMinMax.grp) {
						if (chartId.search(grpName) != -1) yMinMax.grp[grpName] = yMinMax.grp[grpName].concat(yArr)
					}						
				})				
			}
			
			for(var grpName in yMinMax.grp) {
				yMinMax.grp[grpName] = d3.extent( yMinMax.grp[grpName] );
			} //if (i) alert("Number of reused series info: "+i +". "+ reused)
		}
	}
	
	//get the applicable min-max range for a given chart id
	//this could be group min-max, user selected, or the chart's own y-scale range
	main.get = function (chartId,wrapperId) { 
		var tempMinMax = [];
	
		if (yMinMax.active == "user") tempMinMax = yMinMax.user
		else if (!yMinMax.active.length) tempMinMax = yMinMax.self[ chartId.replace('_'+wrapperId,'') ]; //remove wrapperId from chartId
		else { 
			for(i=0; i < yMinMax.active.length; i++) { 
				if (chartId.search( yMinMax.active[i] ) != -1) {  var grp = yMinMax.active[i];
					tempMinMax = yMinMax.grp[ yMinMax.active[i] ]; break;
				}
			} 
		} 
	
		if (!tempMinMax || !tempMinMax.length) tempMinMax = yMinMax.user //apply user specified scale as default
		if (yMinMax.override.min) tempMinMax[0] = yMinMax.override.val[0]
		if (yMinMax.override.max) tempMinMax[1] = yMinMax.override.val[1]
		
		if (tempMinMax[0] > tempMinMax[1]) { 
			if (yMinMax.override.min) tempMinMax[1] = tempMinMax[0] < 0 ? tempMinMax[1] - tempMinMax[0] : tempMinMax[1] + tempMinMax[0]
			else if (yMinMax.override.max) tempMinMax[0] = tempMinMax[1] < 0 ? tempMinMax[0] + tempMinMax[1] : tempMinMax[0] - tempMinMax[1]
		}
		
		var tipExtra = Math.abs((tempMinMax[1] - tempMinMax[0]) * axesTip);
		
		return [ tempMinMax[0] - tipExtra,  tempMinMax[1] + tipExtra ]		
	}
	
	main.grp = function (d) { 
		if (!arguments.length) return yMinMax;
		for(var prop in d) { 
			if (/*prop != "self" &&*/ prop) yMinMax[prop] = d[prop];
		}
	}
	
	return main
}


/*****************************************
				Zoom function
- will set the zoomed[chartId] property of a minMax.y() function (find below)
- needs to call e4 or esd3 update function to trigger rescale, transition
******************************************/

function e4_zoom(minMax, esd3) {
	var update = esd3.update,
		toolTipSet = esd3.toolTipSet,
		zoomXY = [], //saved corner points at the start of zoom area selection
		zoomArea = [], //highlighted zoom area while the other corner is being dragged
		zoomSelectOk = ['plotArea'], //list of classNames that, when clicked, will permit zoom area selection				
		zoomFactor = [1,1] //not really used, see comments for zoomAdj() below
	
	//start of zoom sequence, when a user clicks on a blank plot area
	function main(e) {
		if (zoomSelectOk.indexOf(d3.event.target.className.baseVal) == -1) return;
		toolTipSet(null);
	
		zoomXY = d3.mouse(this); // zoomAdj(d3.mouse(this)),zoomAdj(d3.mouse(this));
		var svg = d3.select(this)
		
		svg.on('mousedown.esd3', null)	
			.on('mousemove', zoomDrag)
			.on("mouseup", zoomIn)
					
		zoomArea = svg.append('rect')
			.attr('x', zoomXY[0]) //- margin.left)
			.attr('y', zoomXY[1]) //- margin.top)
			.attr('width',1)
			.attr('height',1)
			.attr('class','zoomArea')
	}

	
	//adjust zoomArea rendering as the corner is being dragged
	function zoomDrag() {
		var tempXY = d3.mouse(this), // zoomAdj(d3.mouse(this)),zoomAdj(d3.mouse(this)),		
		  x = (tempXY[0] > zoomXY[0]) ? zoomXY[0] : tempXY[0],
			y = (tempXY[1] > zoomXY[1]) ? zoomXY[1] : tempXY[1]
					
		zoomArea
			.attr('x', x)
			.attr('y', y)
			.attr('width', Math.abs(zoomXY[0] - tempXY[0]))
			.attr('height', Math.abs(zoomXY[1] - tempXY[1]))
		
		//if (window.getSelection) window.getSelection().removeAllRanges(); //removes unnecessary highlights
	}
	
	//rescale chart on mouseup when the zoomArea width and height are greater than a given threshold 
	function zoomIn() {	
		//removes unnecessary highlights of axis tick labels, not sure why that gets triggered
		//if (window.getSelection) window.getSelection().removeAllRanges(); 
	
		var stop = d3.mouse(this), // zoomAdj(d3.mouse(this)),
			dx = stop[0] - zoomXY[0], 
			dy = stop[1] - zoomXY[1]; 
		
		if (Math.abs(dx) > 10 && Math.abs(dy) > 10) {
			var chart = this.parentNode, wrapperId = chart.parentNode.id
			
			update(wrapperId, {
				x: d3.extent( [ zoomXY[0], stop[0] ] ), //d3.extent is a shortcut to sort the order of these two points from min to max 
				y: d3.extent( [ zoomXY[1], stop[1] ] )
			}, chart.id.replace("_"+wrapperId, "") );
		}
		/*else { 
			d3.select(this).select('.dataSeries')
				.attr('transform', 'translate('+ dx +','+ dy +')')
		}*/
		
		zoomXY = []
		zoomArea.remove()		
		
		v = d3.select(this)
			.on('mousedown.esd3', main)
			.on('mousemove', null)
			.on('mouseup', null)
			
		toolTipSet(esd3.toolTip())		
	}
	
	main.reset = function () { //rescale to scale ranges prior to zoom-in
		var chart = this.parentNode.parentNode, wrapperId = chart.parentNode.id,
			chartId = chart.id.replace("_"+wrapperId, ""); //alert('481 '+chartId)
		
		d3.select(chart).select('svg')
			.on('mousedown.esd3', main)	
			
		d3.select(chart).selectAll('.zoomResetDiv') //,.zoomResetText')
			.on('click.esd3',null)
			.remove()
		
		delete minMax.y().zoomed[chartId]; 		
		update(wrapperId,0,chartId)
	}
	
	main.ok = function (d) {
		if (!arguments.length) return zoomSelectOk;
		
		if (Array.isArray(d)) zoomSelectOk = d
		else zoomSelectOk.push(d);
		
		return main;
	}
	
	return main;
}
