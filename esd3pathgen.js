/*
Purpose: These are the plot type options/functions. It was separated from 
 the esd3 core to distinguish the shared, multiplot 'management' features 
 from the extensible plot functions. The distinction should hopefully 
 encourage user addition, extensions, or corrections of different plot 
 types.
Author: Edgar Sioson
Date: 2012-11-28
*/

var esd3pathgen = { //the functions for actually plotting selected datasets, by plot-type
	text: {
		draw: function (g, d, wrapperId, update) {
			g.selectAll('text')
				.data(d.dset, d.bindkey)
			.enter().append('text')
				.attr('class', d.className)
				.attr('x', d.x)
				.attr('y', d.y)
				.text(d.text)
		},
		minMax: function (xMinMax, yMinMax, d) {return yMinMax},
		update: function (g,d) {
			var texts = g.selectAll('text')
				.data(d.dset, d.bindkey)
				
			texts.enter().append('text')
				.attr('class', d.className)
				.attr('x', d.x)
				.attr('y', d.y)
				.text(d.text)
				
			var tu = texts.transition().duration('duration' in d ? d.duration : 1000)				
			tu.attr('x', d.x)
			tu.attr('y', d.y)
			tu.text(d.text)
			
			texts.exit().remove()
		}, 
		elemExit: function (g, d, wrapperId) {
			g.remove()
		}
	},
	area: {
		draw: function (g, d, wrapperId, update) { 
			var x = d.x, y0 = d.y0, y1 = d.y1; //d must have properties x, y0, and y1
			
			g._f_ = d3.svg.area()
				.x(typeof x=='function' ? function (obs) {return d.xScale(x(obs))} : function (obs) {return d.xScale(obs[x])})				
				.y0(typeof y0=="function" ? function (obs) {return d.yScale(y0(obs))} : function (obs) {return d.yScale(obs[y0])})
				.y1(typeof y1=="function" ? function (obs) {return d.yScale(y1(obs))} : function (obs) {return d.yScale(obs[y1])})
										
			g.append("path")
				.datum(d.dset)
				.attr("id", "d_"+d.id+"_"+wrapperId)
				.attr("class", d.className)
				.attr("d", g._f_)
		},
		minMax: function (xMinMax, yMinMax, d) { 
			var yArr = [],
				fx = typeof d.x=='function' ? function (obs) {return d.x(obs)} : function (obs) {return +obs[d.x]},				
				fy0 = typeof d.y0=="function" ? function (obs) {return d.y0(obs)} : function (obs) {return +obs[d.y0]},
				fy1 = typeof d.y1=="function" ? function (obs) {return d.y1(obs)} : function (obs) {return +obs[d.y1]};
			
			d.dset.map( function (obs) {						
				var x = fx(obs), y0 = fy0(obs), y1 = fy1(obs);
				
				if (x < xMinMax[0] || x > xMinMax[1]) return;
				yArr.push(y0,y1)
			})
			
			return d3.extent(yArr);
		},
		update: function (g,d) { 
			if (!d.dset.length) {
				g.remove(); return;
			}
			else if (!('_f_' in g)) {
				var x = d.x, y0 = d.y0, y1 = d.y1;
				g._f_ =  d3.svg.area()
					.x(typeof x=='function' ? function (obs) {return d.xScale(x(obs))} : function (obs) {return d.xScale(obs[x])})				
					.y0(typeof y0=="function" ? function (obs) {return d.yScale(y0(obs))} : function (obs) {return d.yScale(obs[y0])})
					.y1(typeof y1=="function" ? function (obs) {return d.yScale(y1(obs))} : function (obs) {return d.yScale(obs[y1])})
			}
			
			g.select('path')
				.datum(d.dset) //, d.bindkey)
				.transition()
				.duration(1000)
				.attr("d", g._f_)
				
			//prevent the use of stale path segment data
			delete d.segList
		},
		elemExit: function (g, d, wrapperId) {
			g.remove()
		},
		ptClosest: function (d, pt, r) { //for highlighting datapoint on hover; //this avoids the need to use hidden points for mouseover; the performance penalty is noticeable with lots of invisible points
			if (!("segList" in d)) d.segList =  d3.event.target.pathSegList;
			var dI = -1, minDist = 2*r;
			
			for(var i=0; i < d.segList.numberOfItems; i++) {
				var L = d.segList.getItem(i);
				if (Math.abs(L.x - pt[0]) < r && Math.abs(L.y - pt[1]) < r) {
					if (Math.abs(L.x - pt[0]) + Math.abs(L.y - pt[1]) <= minDist) {
						minDist = Math.abs(L.x - pt[0]) + Math.abs(L.y - pt[1]);
						dI = i;
					}
				}
				//else if (dI != -1) break; //distance is expected to get bigger with subsequent points
			}
			
			if (dI == -1) return -1 
			else { 
				if (dI < d.dset.length) var obsIndex = dI, y="y1"; 
				else var obsIndex =2*d.dset.length - 1 - dI, y="y0";
				
				return [
					typeof d.x=='function' ? d.x(d.dset[obsIndex]) : d.dset[obsIndex][d.x] , 
					typeof d.y=='function' ? d.y(d.dset[obsIndex]) : d.dset[obsIndex][d.y], 
					obsIndex
				];
			}
		}
	},	
	line: {  //this early plot type name corresponds to svg-polyline or path; 'segment' below corresponds to svg-line
		draw: function (g, d, wrapperId, update) {
			var x = d.x, y = d.y
			
			g._f_ = d3.svg.line()
				.x(typeof x=='function' ? function (obs) {return d.xScale(x(obs))} : function (obs) {return d.xScale(obs[x])})				
				.y(typeof y=="function" ? function (obs) {return d.yScale(y(obs))} : function (obs) {return d.yScale(obs[y])})
			
			g.append("path")
				.attr("id", "d_"+d.id+"_"+wrapperId)
				.attr("class", d.className)
				.datum(d.dset)
				.attr("d", g._f_)
		},
		minMax: function (xMinMax, yMinMax, d) {
			var yArr = [],
				fx = typeof d.x=='function' ? function (obs,i) {return d.x(obs,i)} : function (obs) {return +obs[d.x]};				
				fy = typeof d.y=="function" ? function (obs,i) {return d.y(obs,i)} : function (obs) {return +obs[d.y]};
			
			d.dset.map( function (obs) {						
				var x = fx(obs), y = fy(obs);
				
				if (x < xMinMax[0] || x > xMinMax[1]) return;
				yArr.push(y)
			})
			
			return d3.extent(yArr);
		},
		update: function (g,d) {
			if (!d.dset.length) {
				g.remove(); return;
			}
			else if (!('_f_' in g)) {
				var x = d.x, y = d.y;
				g._f_ = d3.svg.line()
					.x(typeof x=='function' ? function (obs) {return d.xScale(x(obs))} : function (obs) {return d.xScale(obs[x])})				
					.y(typeof y=="function" ? function (obs) {return d.yScale(y(obs))} : function (obs) {return d.yScale(obs[y])})
			}
			
			g.select('path')
				.datum(d.dset) //, d.bindkey)
				.transition()
				.duration('duration' in d? d.duration : 1000)
				.attr("d", g._f_)
				
			//prevent the use of stale path segment data
			delete d.segList
		},
		elemExit: function (g, d, wrapperId) {
			g.remove()
		},
		ptClosest: function (d, pt, r) { //this avoids the need to use hidden points for mouseover; the performance penalty is noticeable with lots of invisible points
			if (!("segList" in d)) d.segList =  d3.event.target.pathSegList;
			if (!d.segList) return;
			var obsIndex = -1; minDist = 2*r;

			for(var i=0; i < d.segList.numberOfItems; i++) {
				var L = d.segList.getItem(i); 
				if (Math.abs(L.x - pt[0]) < r && Math.abs(L.y - pt[1]) < r) {
					if (Math.abs(L.x - pt[0]) + Math.abs(L.y - pt[1]) <= minDist) {
						minDist = Math.abs(L.x - pt[0]) + Math.abs(L.y - pt[1]);
						obsIndex = i;
					}
				}
				else if (obsIndex != -1) break; //distance is expected to get bigger with subsequent points
			} z = obsIndex;		
		
			if (obsIndex == -1) return -1;
			else {
				return [
					typeof d.x=='function' ? d.x(d.dset[obsIndex]) : d.dset[obsIndex][d.x] , 
					typeof d.y=='function' ? d.y(d.dset[obsIndex]) : d.dset[obsIndex][d.y], 
					obsIndex
				];
			}
		}
	},
	pt_circle: {
		draw: function (g, d, wrapperId, update) {						
			var cx = typeof d.cx=='function' ? function (obs,i) {return d.xScale(d.cx(obs,i))} : function (obs) {return d.xScale(obs[d.cx])},
				cy = typeof d.cy=='function' ? function (obs,i) {return d.yScale(d.cy(obs,i))} : function (obs) {return d.yScale(obs[d.cy])};
			
			g.attr('id', d.id+"_"+wrapperId)
				.attr('class', d.className)
				.selectAll('circle')							
			.data(d.dset)
				.enter()
			.append("circle")
				.attr('id', ('idFxn' in d) ? d.idFxn : null)
				.attr("r", d.r)
				.attr("cx", cx)
				.attr("cy", cy)
				.attr('class', d.className)
				.style('fill', d.fill)
				.style('stroke', d.stroke)
		},
		minMax: function (xMinMax, yMinMax, d) {
			var yArr = [],						
				fx = typeof d.cx=='function' ? function (obs,i) {return d.cx(obs,i)} : function (obs) {return obs[d.cx]},			
				fy = typeof d.cy=="function" ? function (obs,i) {return d.cy(obs,i)} : function (obs) {return obs[d.cy]};
			
			d.dset.map( function (obs) {						
				var x = fx(obs), y = fy(obs);
				
				if (x < xMinMax[0] || x > xMinMax[1]) return;
				yArr.push(y)
			})
			
			return d3.extent(yArr);
		},
		update: function (g,d) {
			var cx = typeof d.cx=='function' ? function (obs,i) {return d.xScale(d.cx(obs,i))} : function (obs) {return d.xScale(obs[d.cx])}, 
				cy = typeof d.cy=="function" ? function (obs,i) {return d.yScale(d.cy(obs,i))} : function (obs) {return d.yScale(obs[d.cy])},
				c = g.selectAll('circle').data(d.dset, d.bindkey);
			
			if (typeof d.className == 'string' && d.className.search("pt_hidden") != -1) {
				c.attr("cy", cy)
					.attr("cx", cx)	
					.attr('r',d.r)
			}
			else {				
				c.enter().append("circle")
				.attr('id', ('idFxn' in d) ? d.idFxn : null)
				.attr("cx", cx)
				.attr("cy", cy)
				.attr('class', d.className)
				.style('fill', d.fill)
				.style('stroke',d.stroke)
				
				cu = c
					.style('fill', d.fill)
					.style('stroke',d.stroke)
					.transition()
					.duration(1000)
				
				cu.attr("cy", cy)
				cu.attr("cx", cx)
				cu.attr("r", d.r)

				c.exit().remove();
			}
		},
		elemExit: function (g, d, wrapperId) {
			g.remove()
		},
	},
	pt_rect: { //recommend using pt_pathD 'rect' instead which has smoother transition
		draw: function (g, d, wrapperId) {
			var x = typeof d.x == 'function' ? function (obs,i) {return d.xScale(d.x(obs,i))} : function (obs) {return d.xScale(obs[x]) - d.width/2}, 
				y = typeof d.y == 'function' ? function (obs,i) {return d.yScale(d.y(obs,i))} : function (obs) {return d.yScale(obs[y]) - d.height/2};
			
			g.attr('id', d.id+"_"+wrapperId)
				.attr('class', d.className)
				.selectAll('rect')							
			.data(d.dset)
				.enter()
			.append("rect")
				.attr("x", x)
				.attr("y", y)
				.attr("width", d.width)
				.attr("height", d.height)
				.attr("class", d.className)
				.style('fill', d.fill)
				.style('stroke',d.stroke)				
		},
		minMax: function (xMinMax, yMinMax, d) {
			var yArr = [],
				fx = typeof d.x == 'function' ? function (obs,i) {return d.x(obs,i)} : function (obs) { return obs[d.x] }, 
				fy = typeof d.y == 'function' ? function (obs,i) {return d.y(obs,i)} : function (obs) { return obs[d.y] };
			
			d.dset.map( function (d) {						
				var x = fx(obs), y = fy(obs);
				
				if (x < xMinMax[0] || x > xMinMax[1]) return;
				 yArr.push(d[y])
			})
			
			return d3.extent(yArr);
		},
		update: function (g,d) { 						
			var x = typeof d.x == 'function' ? function (obs,i) {return d.xScale(d.x(obs,i))} : function (obs) {return d.xScale(obs[x]) - d.width/2}, 
				y = typeof d.y == 'function' ? function (obs,i) {return d.xScale(d.y(obs,i))} : function (obs) {return d.yScale(obs[y]) - d.height/2};						
			
			var r = g.selectAll('rect')
				.style('fill', d.fill)
				.style('stroke',d.stroke)
				.transition()
				.duration(1000)
				
				r.attr("y", y)
				r.attr("x", x)					
		},
		elemExit: function (g, d, wrapperId) {
			g.remove()
		}		
	},
	pt_pathD: { //handles arbitrary shapes defined as pathD centered on x,y
		draw: function (g, d, wrapperId) {
			//if (!("pathD" in d)) alert(JSON.stringify(d));
			var x = d.x, y = d.y,
				fxn = typeof x=='function' && typeof y=='function' ? function (obs) {return "translate("+ d.xScale(x(obs)) +","+ d.yScale(y(obs)) +")"}
					: typeof x=='function' ? function (obs) {return "translate("+ d.xScale(x(obs)) +","+ d.yScale(obs[y]) +")"}
					: typeof y=='function' ? function (obs) {return "translate("+ d.xScale(obs[x]) +","+ d.yScale(y(obs)) +")"}
					: function (obs) {return "translate("+ d.xScale(obs[x]) +","+ d.yScale(obs[y]) +")"}
			
			
			if (d.pathD.substr(0,1) != "M") d.pathD = d3.svg.symbol().type(d.pathD)
			
			g.attr('id', d.id+"_"+wrapperId)
					.attr('class', d.className)
					.selectAll('path')							
				.data(d.dset)
					.enter()
				.append("path")
					.attr("class", d.className)
					.attr("d", d.pathD)
					.attr("transform", fxn)
					.style('fill', d.fill)
					.style('stroke',d.stroke)
		},
		minMax: function (xMinMax, yMinMax, d) { 
			var yArr = [],
				fx = typeof d.x=='function' ? function (obs,i) {return d.x(obs,i)} : function (obs) {return +obs[d.x]},			
				fy = typeof d.y=="function" ? function (obs,i) {return d.y(obs,i)} : function (obs) {return +obs[d.y]};
			
			d.dset.map( function (obs) {				
				var x = fx(obs), y = fy(obs); 
				
				if (x < xMinMax[0] || x > xMinMax[1]) return;
				yArr.push(y); 
			}); 
			
			return d3.extent(yArr);
		},
		update: function (g,d) { 										
			var x = d.x, y = d.y;
			
			var paths = g.selectAll('path')
				//.filter( d.filter )
				.data(d.dset)
			
			paths.enter()
				.append("path")
				.attr("class", d.className)
				.attr("d", d.pathD)
				.style('fill', d.fill)
				.style('stroke',d.stroke)
			
			var t = paths.transition().duration(1000),
				fxn = typeof x=='function' && typeof y=='function' ? function (obs) {return "translate("+ d.xScale(x(obs)) +","+ d.yScale(y(obs)) +")"}
					: typeof x=='function' ? function (obs) {return "translate("+ d.xScale(x(obs)) +","+ d.yScale(obs[y]) +")"}
					: typeof y=='function' ? function (obs) {return "translate("+ d.xScale(obs[x]) +","+ d.yScale(y(obs)) +")"}
					: function (obs) {return "translate("+ d.xScale(obs[x]) +","+ d.yScale(obs[y]) +")"}
			
			t.attr("transform",  fxn)
				
			paths.exit().remove();
		},
		elemExit: function (g, d, wrapperId) { 
			g.remove()
		}			
	},
	stack_v: {
		draw: function (g, d, wrapperId, update) {			
			g.attr('transform', update ? 'translate(2000,0)' : d.translate)
					.attr('class', d.className)
			
			var cells = g.selectAll('rect')
				.data(d.dset, d.bindkey)
				.enter()
			
			if ('cellRect' in d) 
				cells.append('rect') 
				.attr('id', d.cellRect.id)
				.attr('className', d.cellRect.className)
				.attr('x', d.cellRect.x)
				.attr('y', d.cellRect.y) 
				.attr('height', d.height)
				.attr('width', d.width)
				.attr('class', d.cellRect.className)
				.style('fill', d.fill)
				.style('stroke', d.stroke)
			
			if ('cellText' in d) 
				cells.append('text')
				.attr('id', d.cellText.id)
				.attr('class', d.cellText.className)
				.attr('text-anchor', d.cellText.anchor)
				.attr('x', d.cellText.x)
				.attr('y', d.cellText.y)
				//.style('font-size', d.cellText.font.size)
				.style('fill', d.cellText.fill)
				.text(d.cellText.val)
				
			if ('colLabel' in d) {
				if ('fxn' in d.colLabel) d.colLabel.fxn(g,d)
				else if ('text' in d.colLabel) {
					g.append('text')
						.attr('id', d.colLabel.id)
						.attr('x', d.colLabel.x)
						.attr('y', d.colLabel.y)
						.attr('class', d.colLabel.className)
						.text(d.colLabel.text)
				}
			}
			
			if (update) g.transition().duration(d.duration)
				.attr('transform', d.translate);
		},
		minMax: function (xMinMax, yMinMax, d) { return yMinMax },
		update: function (g,d) { 
			if ('cellRect' in d) {
				var cells = g.selectAll('rect')
					.data(d.dset, d.bindkey)
					.attr('id', d.cellRect.id)
					.style('fill', d.fill)
			
				cells.enter().append('rect')
					.attr('id', d.cellRect.id)
					.attr('className', d.cellRect.className)
					.attr('x', d.cellRect.x)
					.attr('y', 'yInit' in d.cellRect ? d.cellRect.yInit : d.cellRect.y) 
					.attr('height', d.height)
					.attr('width', d.width)
					.attr('class', d.cellRect.className)
					.style('fill', d.fill)
					.style('stroke', d.stroke)
					.attr('id', d.cellRect.id)
					.style('fill', d.fill)
				
				/*cells.exit()
					.attr('x', d.cellRect.x)
					.attr('y', d.cellRect.y) 
					.style('fill', d.fill)
					.style('stroke', d.stroke)
					.attr('id', d.cellRect.id)
					.style('fill', d.fill)*/
			}
															
			if ('cellText' in d) {
				texts = g.selectAll("."+d.cellText.className)
					.data(d.dset, d.bindkey)
					.attr('id', d.cellText.id)
					.text(d.cellText.val)	

				texts.enter().append('text')
					.attr('id', d.cellText.id)
					.attr('class', d.cellText.className)
					.attr('text-anchor', d.cellText.anchor)
					.attr('x', d.cellText.x)
					.attr('y', 'yInit' in d.cellText ? d.cellText.yInit : d.cellText.y)
					//.style('font-size', d.cellText.font.size)
					.style('fill', d.cellText.fill)
					.text(d.cellText.val)
			}
		
			var t = g.transition()
				.duration(d.duration)
				
			if (d.transSelect.col)	t.attr('transform', d.translate)
			
			if (d.transSelect.cell && 'cellRect' in d) { //console.log(375)
				var cellRect = t.selectAll('rect')
				cellRect.attr("y", d.cellRect.y)
				cellRect.attr("x", d.cellRect.x)
			}
			
			if (d.transSelect.text && 'cellText' in d) { //console.log(380)
				var cellText = t.selectAll("."+d.cellText.className)
				cellText.attr("y", d.cellText.y)		
				cellText.attr("x", d.cellText.x)
			}
		},
		elemExit: function (g, d, wrapperId) { 
			g.transition().duration(2000)
				.attr('transform', 'translate(2000,0)') //function (d) { return (d.iso3 != 99) ? 'translate(3000,0)' : 'translate(10,15)'})	
				.remove()
		}
	},
	segment: {
		draw: function (g, d, wrapperId) {
			var p = d.prop, //the dataset property to be mapped; this property must be an object with {name1: val1, name2: ...}
				 x=0; 
			
			g.attr('transform', d.translate)
					.attr('class', d.className)
			
			var lines = g.selectAll('line')
				.data(d.dset)
			.enter().append('line')							
				.attr('id', d.line.id)
				.attr('class', d.className)
				.attr('x1', d.x1)
				.attr('y1', d.y1)
				.attr('x2', d.x2)
				.attr('y2', d.y2)
				.style('stroke', d.stroke)
			
			if ('style' in d) {
				for(var s in d.style) lines.style( s, d.style[s] );
			}
			
			/*
			//this is an experiment to restructure the metadata format 
			//to more closely align with d3's approach -- it works but not sure
			//as of 2012-11-16 whether to pursue this some more
			for(var elem in d.appends) {
				var e = lines.append(elem);
				for(var a in d.appends[elem].attrs) {alert(a); e.attr(a, d.appends[elem].attrs[a]);}
				if ('styles' in d.appends[elem]) {
					for(var s in d.appends[elem].styles) {alert(s); e.style(s, d.appends[elem].styles[s]);}
				}
			}*/
		},
		minMax: function (xMinMax, yMinMax, d) { return yMinMax },
		update: function (g,d) { //if ($('#region').val()==10) alert(422) 					
			var lines = g.selectAll('line').data(d.dset, d.bindkey)
			
			if ('style' in d) {
				for(var s in d.style) lines.style( s, d.style[s] );
			}
				
			var	L = lines.transition()
				.duration(d.duration)
														
			L.attr("y1", d.y1)
			L.attr("y2", d.y2)
			L.attr("x1", d.x1)
			L.attr("x2", d.x2)
		},
		elemExit: function (g, d, wrapperId) {
			g.remove()
		}
	},
	generic: {
		draw: function (g, d, wrapperId, update) {
			var node = g.selectAll('elems')
				.data(d.dset, d.bindkey)
				.enter()
				
			for(var tag in d.elems) {
				var elem = node.append(tag)
				
				for(var attrName in d.elems.attrs) 
					var attr = d.elems.attrs[attrName]
					
					elem
					.transition()
					.delay(attr.delay)
					.duration(attr.duration)
					.attr(attrName, attr.val)
			}
		},
		minMax: function (xMinMax, yMinMax, d) { return yMinMax },
		update: function (g,d) { //if ($('#region').val()==10) alert(422) 					
			var lines = g.selectAll('line').data(d.dset, d.bindkey)
			
			if ('style' in d) {
				for(var s in d.style) lines.style( s, d.style[s] );
			}
				
			var	L = lines.transition()
				.duration(d.duration)
														
			L.attr("y1", d.y1)
			L.attr("y2", d.y2)
			L.attr("x1", d.x1)
			L.attr("x2", d.x2)
		}, 
		elemExit: function (g, d, wrapperId) {
			g.remove()
		}
	}
}
